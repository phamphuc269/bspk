<?php if(have_rows('module')): ?>
    <?php while(have_rows('module')) : the_row(); ?>
        <?php 
            if(get_row_layout() == 'section-hero-module'): 

                echo get_template_part( 'template-parts/modules/section-hero-module' );
                
            endif;
        ?>
    <?php endwhile; ?>
<?php endif; ?>
