<?php

    /*----------------------------

    Theme Setup

    ------------------------------*/

    define ('sticky_home' , esc_url( home_url( '/' ) ));

    define ('sticky_uri' , get_template_directory_uri());

    define ('sticky_dir' , get_template_directory());

    define ('sticky_inc', sticky_uri . '/inc');



    /*----------------------------

	Basic

	------------------------------*/

	function sticky_theme_setup() {



		load_theme_textdomain( 'nsc' );



		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );



        register_nav_menus( array(

			'main'    => __( 'Header - Main Menu', 'web Mẫu' ),

		) );



        add_theme_support( 'html5', array(

            'format',

                'comment-form',

                'comment-list',

                'gallery',

                'caption',

            ) );

    

            add_theme_support( 'woocommerce' );

    }

    add_action( 'after_setup_theme', 'sticky_theme_setup' );



    /*----------------------------

	Use old editor

	------------------------------*/

    add_filter('use_block_editor_for_post', '__return_false');



    /*----------------------------

	Disable Admin Toolbar

	------------------------------*/

    add_filter( 'show_admin_bar', '__return_false' );



    /*----------------------------

	Insert JS & CSS

	------------------------------*/

    function sticky_css_js() {

        wp_enqueue_style( 'slick', sticky_uri . '/libs/slick/slick.css', array(), '' );

        wp_enqueue_style( 'slick-theme', sticky_uri . '/libs/slick/slick-theme.css', array(), '' );

        wp_enqueue_style( 'theme-style', sticky_uri . '/assets/css/theme.css', array(), '' );

        wp_enqueue_style( 'style', sticky_uri . '/style.css', array(), '' );


        wp_enqueue_script('jquery-js', sticky_uri . '/assets/js/jquery.min.js', '', '', true);

        wp_enqueue_script('slick-js', sticky_uri . '/libs/slick/slick.min.js', '', '', true);

        wp_enqueue_script('stickyjs', sticky_uri . '/assets/js/main.js', '', '', true);

    }

    add_action( 'wp_enqueue_scripts', 'sticky_css_js' );




     // Option page

     if( function_exists('acf_add_options_page') ) {


        acf_add_options_page(array(

            'page_title' 	=> 'Trang quản trị',

            'menu_title'	=> 'Trang quản trị',

            'menu_slug' 	=> 'theme-general-settings',

            'capability'	=> 'edit_posts',

            'redirect'		=> false

        ));

        

    }
